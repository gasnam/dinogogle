package com.gameLauncher.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameobject.game.Dino;
import com.logicgame.game.GameStateManager;
import com.logicgame.game.MenuState;


public class DinoGogle extends ApplicationAdapter {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 480;

    public static final String TITLE = "DinoGogle";

    private GameStateManager gsm;
    private SpriteBatch batch;


    private Dino dino;

    @Override
    public void create() {
        batch = new SpriteBatch();
        gsm = new GameStateManager();
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        gsm.push(new MenuState(gsm));
    }

    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gsm.update(Gdx.graphics.getDeltaTime());
        gsm.render(batch);

    }

    @Override
    public void dispose () {
        super.dispose();
    }


}

