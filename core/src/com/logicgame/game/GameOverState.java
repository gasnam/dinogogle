package com.logicgame.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Locale;

public class GameOverState extends State {

    private Texture background;
    private Texture GameOverWindow;
    int score;
    BitmapFont scoreFont;


    public GameOverState(GameStateManager gsm, int score) {
        super(gsm);
        background = new Texture("FON.png");
        GameOverWindow = new Texture("ButtonGameOver.png");
        this.score = score;
        scoreFont = new BitmapFont();
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.justTouched()) {
            gsm.set(new MenuState(gsm));
        }
    }

    @Override
    public void update(float deltaTime) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.begin();

        batch.draw(background, 0, 0, background.getWidth(), background.getHeight());
        batch.draw(GameOverWindow, (background.getWidth() / 2) - (GameOverWindow.getWidth() / 2), (background.getHeight() / 2) - (GameOverWindow.getHeight() / 2));
        GlyphLayout scoreLayout = new GlyphLayout(scoreFont, " " + score);
        scoreFont.draw(batch, scoreLayout, background.getWidth() / 2 - scoreLayout.width+25, background.getHeight() / 2 - scoreLayout.height + 50);
        batch.end();
    }

    @Override
    public void dispose() {

    }

}

