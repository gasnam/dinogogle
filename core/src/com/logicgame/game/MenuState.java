package com.logicgame.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameLauncher.game.DinoGogle;
import com.gameobject.game.Dino;


public class MenuState extends State {

    private Texture background;
    private Texture buttonPlay;

    public MenuState(GameStateManager gsm) {
        super(gsm);
        background = new Texture("FON.png");
        buttonPlay = new Texture("ButtonPlay.png");
        camera.setToOrtho(false, DinoGogle.WIDTH, DinoGogle.HEIGHT);
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()){
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float deltaTime) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, 0,0, background.getWidth(), background.getHeight());
        batch.draw(buttonPlay, (background.getWidth()/ 2) - (buttonPlay.getWidth()/2), (background.getHeight()/2) - (buttonPlay.getHeight()/2) );
        batch.end();
    }

    @Override
    public void dispose() {

    }
}
