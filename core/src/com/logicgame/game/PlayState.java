package com.logicgame.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gameLauncher.game.DinoGogle;
import com.gameobject.game.Dino;
import com.gameobject.game.Thorns;

import java.util.ArrayList;
import java.util.Random;

public class PlayState extends State {

    // Константы для изменение скорости на будущее (пока не работает, а выкидывать жалко, д и ктомуже могу потерять)
    public static final int DEFAULT_SPEED = 80;
   // public static final int ACCELERATION = 50; ....для ускорения
    public static final int GOAL_REACH_ACCELERATION = 200;
    public static final float MIN = 1.8f;
    public static final float MAX = 3.3f;

    private Dino dino;
    private Texture background;
    // Необходимые переменные
    private float y1, y2;
    int speed;
    int goalSpeed;
    float imageScale;

    BitmapFont scoreFont;
    int score;
    int time;

    private Random rand;

    private ArrayList<Thorns> thornsArray;
    float spawnTime;

    public PlayState(GameStateManager gsm) {
        super(gsm);
        int dsm = 0;
        dino = new Dino(50, 100);
        camera.setToOrtho(false, DinoGogle.WIDTH, DinoGogle.HEIGHT);
        background = new Texture("BackgroundGame.png");
        y1 = 0;
        y2 = background.getWidth();
        speed = 0;
        goalSpeed = DEFAULT_SPEED;
        imageScale = 0;
        time = 10;

        score = 0;
        scoreFont = new BitmapFont();

        thornsArray = new ArrayList<Thorns>();
        rand =new Random();
        spawnTime = rand.nextFloat() * (MAX - MIN) + MIN;

    }

    protected void handleInput() {
        if(Gdx.input.justTouched())
            if(dino.getPosition().y > 70){
                Gdx.input.isKeyJustPressed(1);
            }else {
                dino.jump();
            }
    }

    public void update(float deltaTime) {

        // Для изменение скорости прокрутки но покачто она одна на будущее
        if (speed < goalSpeed) {
            speed += GOAL_REACH_ACCELERATION * deltaTime;
            if (speed > goalSpeed) speed = goalSpeed;

        } else if (speed > goalSpeed) {
            speed -= GOAL_REACH_ACCELERATION * deltaTime;
            if (speed < goalSpeed)
                speed = goalSpeed;
        }

        y1 -= speed * deltaTime;
        y2 -= speed * deltaTime;

        time -=1;
        if(time<=0){
            score+=1;
            time=10;
        }
        handleInput();
        dino.update(deltaTime);

        camera.position.x = dino.getPosition().x + (Gdx.graphics.getWidth()/5)- 10;

        spawnTime -=deltaTime;
        if(spawnTime <= 0){
            spawnTime = rand.nextFloat() * (MAX - MIN) + MIN;
            thornsArray.add(new Thorns(800,70));
        }
        ArrayList<Thorns> thornsDelete = new ArrayList<Thorns>();
        for(Thorns thorns: thornsArray){
            thorns.update(deltaTime);
            if(thorns.remove){
                thornsDelete.add(thorns);
            }
            if (thorns.collides(dino.getBounds())){
                gsm.set(new GameOverState(gsm,score));
            }
        }
       thornsArray.removeAll(thornsDelete);
        camera.update();



    }

    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        // перемешение изображения 1 за изображение 2 и наоборот
        if (y1 + background.getWidth() * imageScale <= 0)
            y1 = y1 + background.getWidth() * imageScale;
        if (y2 + background.getWidth() * imageScale <= background.getWidth())
            y2 = y2 + background.getWidth() * imageScale;


        resize(1000, 500);

        batch.begin();
        // отрисовка заднего фона
        batch.draw(background, y1, 0, background.getWidth() * imageScale, background.getHeight());
        batch.draw(background, y2, 0, background.getWidth() * imageScale, background.getHeight());
//счетчик
        GlyphLayout scoreLayout = new GlyphLayout(scoreFont, "" + score);
        scoreFont.draw(batch,scoreLayout,background.getWidth()/2- scoreLayout.width,background.getHeight() - scoreLayout.height - 10);



        batch.draw(dino.getDino(), dino.getPosition().x, dino.getPosition().y);

        for(Thorns thorns : thornsArray){
            batch.draw(thorns.getThorns(), thorns.getPosThorns().x, thorns.getPosThorns().y);

        }
        batch.end();

    }

    public void dispose(){

    }

    //  (Объяснить на словах сложно)Необхосимо для плавной крокрутки экрана
    public void resize(int width, int height) {
        imageScale = width / background.getWidth();
    }
}
