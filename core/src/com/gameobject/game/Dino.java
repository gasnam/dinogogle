package com.gameobject.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.gameLauncher.game.DinoGogle;

public class Dino {

    private static final int MOVEMENT = 200;
    private static final int GRAVITY_UP = 1;
    private static final int GRAVITY = -4;

    private Vector3 position;
    private Vector3 velocity;
    private Animation dinoAnimation;
    private Animation dinoJumpAnimation;
    private Rectangle bounds;

    private Texture dinoPicture;

    public Dino(int x, int y){
        position = new Vector3(x, y, 0);
        velocity = new Vector3(0, 0 ,0);

        Texture texture = new Texture("DinoAnimation.png");
        Texture textureAnimation = new Texture("AnimationJump.png");

        dinoAnimation = new Animation(new TextureRegion(texture), 3, 0.5f);
        dinoJumpAnimation = new Animation(new TextureRegion(textureAnimation), 5, 0.6f);

        bounds = new Rectangle(x,y, 32, 32);
    }

    public Vector3 getPosition() {
        return position;
    }

    public TextureRegion getDino(){
        if(velocity.y > 70){
            return dinoJumpAnimation.getFrame();
        }else {
            return dinoAnimation.getFrame();
        }
    }

    public void update(float dt){
        if(velocity.y > 70){
            dinoJumpAnimation.update(dt);
        }else {
            dinoAnimation.update(dt);
        }




        velocity.add(0, GRAVITY, 0);
        position.add(0, velocity.y*dt, 0);
        if(position.y < 70){
            position.y = 70;
        }
        if(position.y > DinoGogle.HEIGHT / 3){
            position.y = DinoGogle.HEIGHT / 3;
        }

        bounds.setPosition(position.x, position.y);


    }
    public void jump(){
        velocity.y = 180;
        velocity.add(0, GRAVITY_UP, 0);
    }

    public void dispose(){
        dinoPicture.dispose();

    }

    public Rectangle getBounds(){
        return bounds;
    }
}
