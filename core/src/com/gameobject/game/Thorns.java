package com.gameobject.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;
import java.util.Random;


public class Thorns {


    public static final int THORNS_WIDTH = 60;

    private Rectangle boundsThorns;

    private Texture thorns;
    private Vector3 posThorns;

    Array<Rectangle> thorn;

    public boolean remove = false;

    public Texture getThorns() {
        return thorns;
    }

    public Vector3 getPosThorns() {
        return posThorns;
    }

    public Thorns(float x, int y){

        thorns = new Texture("thorns.png");
        posThorns = new Vector3(x, y, 0);

        boundsThorns = new Rectangle(posThorns.x, posThorns.y, 32, 32);
    }

    public void update(float dt){
        posThorns.add(-100 * dt, 0, 0);
        boundsThorns.setPosition(posThorns.x, posThorns.y);
        if(posThorns.x <= 0){
            remove = true;
        }
    }

    public void reposition(float x){
        posThorns.set(x ,70, 0);

    }

    public boolean collides(Rectangle player){
        return player.overlaps(boundsThorns);
    }

    public void dispose(){
        thorns.dispose();

    }
}
